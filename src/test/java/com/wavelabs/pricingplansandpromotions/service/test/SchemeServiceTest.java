package com.wavelabs.pricingplansandpromotions.service.test;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wavelabs.pricingplansandpromotions.model.Scheme;
import com.wavelabs.pricingplansandpromotions.model.ZipCode;
import com.wavelabs.pricingplansandpromotions.repository.SchemeRepository;
import com.wavelabs.pricingplansandpromotions.service.SchemeService;
import com.wavelabs.pricingplansandpromotions.service.ZipCodeService;

@RunWith(MockitoJUnitRunner.class)
public class SchemeServiceTest {
	
	@Mock
	SchemeRepository schemeRepo;
	
	@InjectMocks
	SchemeService schemeService;
	
	@Mock
	ZipCodeService zipCodeService;

	@Test
	public void testAddScheme() {
		Scheme scheme = DataBuilder.getScheme();
		when(schemeRepo.save(any(Scheme.class))).thenReturn(scheme);
		Scheme scheme1 = schemeService.addScheme(scheme);
		Assert.assertEquals(scheme, scheme1);
	}
	
	@Test
	public void testGetScheme(){
		Scheme scheme = DataBuilder.getScheme();
		when(schemeRepo.findOne(anyInt())).thenReturn(scheme);
		Scheme scheme1 = schemeService.getScheme(1);
		Assert.assertEquals(scheme, scheme1);
	}
	
	/*@Test
	public void testGetRandomScheme(){
		ZipCode zip = DataBuilder.getZipCode();
		when(zipCodeService.getZipCode(anyInt())).thenReturn(zip);
		Scheme scheme1 = schemeService.getRandomScheme(1,1);
		Assert.assertNotEquals(scheme1, null);
	}*/
}
