package com.wavelabs.pricingplansandpromotions.service.test;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;

import org.hibernate.Query;
import org.hibernate.Session;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wavelabs.pricingplansandpromotions.model.Plan;
import com.wavelabs.pricingplansandpromotions.repository.PlanRepository;
import com.wavelabs.pricingplansandpromotions.service.PlanService;
import com.wavelabs.pricingplansandpromotions.service.SchemeService;


@RunWith(MockitoJUnitRunner.class)
public class PlanServiceTest {

	@Mock
	PlanRepository planRepo;
	
	@Mock
	PlanService planServiceMock;

	@InjectMocks
	PlanService planService;
	
	@InjectMocks
	SchemeService schemeService;
	
	@Mock
	Session session;
	
	@Mock
	Query query;

	/*@Test
	public void testAddPlan() {
		Plan plan = DataBuilder.getPlan();
		Scheme scheme = DataBuilder.getScheme();
		when(schemeService.getScheme(anyInt())).thenReturn(scheme);
		Plan plan1 = planService.addPlan(plan, 1);
		Assert.assertEquals(plan, plan1);
	}*/
	
	@Test
	public void testGetPlan(){
		Plan plan = DataBuilder.getPlan();
		when(planRepo.findOne(anyInt())).thenReturn(plan);
		Plan plan1 = planService.getPlan(1);
		Assert.assertEquals(plan.getId(), plan1.getId());
	}
	
	@Test
	public void testIsPlanIf(){
		Plan plan=DataBuilder.getPlan();
		when(planServiceMock.getPlan(anyInt())).thenReturn(plan);
		boolean result=planService.isPlan(1);
		Assert.assertEquals(true, result);
	}
	
	@Test
	public void testIsPlanElse(){
		Plan plan=DataBuilder.getFreeTrial();
		when(planServiceMock.getPlan(anyInt())).thenReturn(plan);
		boolean result=planService.isPlan(0);
		Assert.assertEquals(false, result);
	}
}
