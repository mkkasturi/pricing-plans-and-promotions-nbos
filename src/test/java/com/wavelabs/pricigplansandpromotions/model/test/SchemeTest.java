package com.wavelabs.pricigplansandpromotions.model.test;

import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

import com.wavelabs.pricingplansandpromotions.model.Plan;
import com.wavelabs.pricingplansandpromotions.model.Scheme;

public class SchemeTest {

	@Test
	public void testGetId(){
		Scheme scheme = new Scheme();
		scheme.setId(5);
		Assert.assertEquals(5, scheme.getId());
	}
	
	
	@Test
	public void testGetName(){
		Scheme scheme = new Scheme();
		scheme.setName("Scheme A");
		Assert.assertEquals("Scheme A", scheme.getName());
	}
	
	@Test
	public void testGetIsActive(){
		Scheme scheme = new Scheme();
		scheme.setIsActive((byte) 0);
		Assert.assertEquals(0, scheme.getIsActive());
		
	}
	
	@Test
	public void testGetFreeTrial(){
		Scheme scheme = new Scheme();
		scheme.setFreeTrial((byte) 1);
		Assert.assertEquals(1, scheme.getFreeTrial());
	}
	
	@Test
	public void testGetPlans(){
		Scheme scheme = new Scheme();
		Plan plan1 = new Plan();
		Plan plan2 = new Plan();
		Plan plan3 = new Plan();
		Plan plan4 = new Plan();
		Set<Plan> plans = new HashSet<Plan>();
		plans.add(plan1);
		plans.add(plan2);
		plans.add(plan3);
		plans.add(plan4);
		scheme.setPlans(plans);
		Assert.assertEquals(plans, scheme.getPlans());
	}
}
