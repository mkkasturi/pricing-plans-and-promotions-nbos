package com.wavelabs.pricingplansandpromotios.filterCheckings;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wavelabs.pricingplansandpromotions.model.User;
import com.wavelabs.pricingplansandpromotions.repository.UserRepository;



@Component
public class UserCheckings {

	private static final Logger logger = Logger.getLogger(UserCheckings.class);


	@Autowired
	private UserRepository userRepo;

	public boolean isUserExist(String uuid, String tenantId) {
		try {
			User user = userRepo.findByUuidAndTenantId(uuid, tenantId);
			return (user != null);
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}
}
