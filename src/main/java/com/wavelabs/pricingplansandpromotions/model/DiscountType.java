package com.wavelabs.pricingplansandpromotions.model;

public enum DiscountType {
	PERCENTAGE, AMOUNT;
}
