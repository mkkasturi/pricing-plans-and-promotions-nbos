package com.wavelabs.pricingplansandpromotions.model;

public enum UserType {
	provider, receiver
}
