package com.wavelabs.pricingplansandpromotions.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "zip_code")
public class ZipCode {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	@Column(name = "zip_code", nullable = false, unique=true)
	private String zipCode;

	@Column(name = "city")
	private String city;

	@Column(name = "state")
	private String state;

	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
	@JoinTable(name = "zipcode_scheme", joinColumns = { @JoinColumn(name = "scheme_id") }, inverseJoinColumns = {
			@JoinColumn(name = "zipcode_id") })
	private Set<Scheme> schemes;

	public ZipCode() {

	}

	public Set<Scheme> getSchemes() {
		return schemes;
	}

	public void setSchemes(Set<Scheme> schemes) {
		this.schemes = schemes;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
}
