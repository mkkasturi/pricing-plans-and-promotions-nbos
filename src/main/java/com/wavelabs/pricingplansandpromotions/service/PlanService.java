package com.wavelabs.pricingplansandpromotions.service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;
import org.apache.log4j.Logger;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wavelabs.pricingplansandpromotions.model.Plan;
import com.wavelabs.pricingplansandpromotions.model.PromoCode;
import com.wavelabs.pricingplansandpromotions.model.Scheme;
import com.wavelabs.pricingplansandpromotions.model.User;
import com.wavelabs.pricingplansandpromotions.repository.PlanRepository;
import com.wavelabs.pricingplansandpromotions.repository.PromoCodeRepository;
import com.wavelabs.pricingplansandpromotions.repository.SchemeRepository;

@Component
public class PlanService {
	@Autowired
	PlanRepository planRepo;

	@Autowired
	PlanService planService;

	@Autowired
	UserService userService;

	@Autowired
	SchemeService schemeService;

	@Autowired
	SchemeRepository schemeRepo;

	@Autowired
	PromoCodeService promoCodeService;

	@Autowired
	PromoCodeRepository promoCodeRepo;

	private final static Logger LOGGER = Logger.getLogger(PlanService.class.getName());

	public Plan getPlan(int planId) {
		return planRepo.findOne(planId);
	}

	@Transactional
	public boolean addPlan(Plan plan, int schemeId) {
		try {
			Scheme scheme = schemeService.getScheme(schemeId);
			plan.setScheme(scheme);
			planRepo.save(plan);
			return true;
		} catch (Exception e) {
			LOGGER.error(e);
			return false;
		}
	}

	public Plan deletePlan(int planId) {
		Plan plan = planRepo.findOne(planId);
		if (plan != null) {
			planRepo.delete(planId);
			return plan;
		} else {
			return null;
		}
	}

	public boolean isPlan(int planId) {
		Plan plan = planService.getPlan(planId);
		if (plan.getPrice() <= 0 && plan.getDurationInMonths() == 0) {
			return false;
		} else {
			return true;
		}
	}

	public Plan getPlanAfterFreeTrial(int planId) {
		if (!(isPlan(planId))) {
			Plan freeTrial = planService.getPlan(planId);
			int schemeId = freeTrial.getScheme().getId();
			List<Plan> plans = planRepo.getPlansByOrder(schemeId);
			return plans.get(1);
		} else {
			return null;
		}
	}

	public User makePayment(double amount, int userId, int promoCodeId, int planId) {
		PromoCode promoCode = promoCodeService.getPromoCode(promoCodeId);
		String code = promoCode.getCode();
		User user = userService.getUser(userId);
		Object[] amountCalculated = promoCodeService.getAmount(planId, code, userId);
		double billAmount = (double) amountCalculated[0];
		if (billAmount > 0) {
			Plan plan = planService.getPlan(planId);
			if (amount >= billAmount) {
				user.setPlan(plan);
				int noOfDays = plan.getDurationInMonths() * 30;
				Date planRenewalDate = DateService.addDaysToDate(noOfDays);
				user.setPlanRenewalDate(planRenewalDate);
				user.setAmountPaidForPresentPlan(amount);
				Set<PromoCode> promoCodes = user.getUsedPromoCodes();
				promoCodes.add(promoCode);
				userService.updateUser(user);
			} else {
				LOGGER.info("Transaction Failed!");
			}
		} else {
			LOGGER.info("Something went wrong!");
		}
		return user;
	}

	public User freeTrial(int planId, int userId, String code) {
		PromoCode promoCode = promoCodeRepo.findByCode(code);
		Plan plan = planService.getPlan(planId);
		Plan nextPlan = getPlanAfterFreeTrial(planId);
		int noOfDays = plan.getNoOfDaysForFreeTrial();
		Date expiryDate = DateService.addDaysToDate(noOfDays);
		User user = userService.getUser(userId);
		user.setPlan(plan);
		user.setNextSubscriptionPlan(nextPlan);
		user.setFreeTrialExpiryDate(expiryDate);
		user.setFreeTrialPromoCode(promoCode);
		userService.updateUser(user);
		return user;
	}

	public void renewalPlan() {

		List<User> users = userService.getAllUsers();
		for (User user : users) {
			if (user.getPlan() != null) {
				Plan plan = user.getPlan();
				int planId = plan.getId();
				Date today = Calendar.getInstance().getTime();
				if (isPlan(planId)) {
					Date planRenewalDate = user.getPlanRenewalDate();
					if (today.after(planRenewalDate)) {
						int noOfDays = plan.getDurationInMonths() * 30;
						Date newPlanRenewalDate = DateService.addDaysToDate(noOfDays);
						user.setPlanRenewalDate(newPlanRenewalDate);
						userService.updateUser(user);
					}
				} else {
					Date expiry = user.getFreeTrialExpiryDate();
					if (today.after(expiry)) {
						Plan nextPlan = user.getNextSubscriptionPlan();
						PromoCode promoCode = user.getFreeTrialPromoCode();
						String code = promoCode.getCode();
						Object[] amountCalculated = promoCodeService.getAmount(nextPlan.getId(), code, user.getId());
						double price = (double) amountCalculated[0];
						if (price > 0) {
							user.setFreeTrialExpiryDate(null);
							user.setPlan(nextPlan);
							user.setNextSubscriptionPlan(null);
							user.setFreeTrialPromoCode(null);
							int noOfDays = nextPlan.getDurationInMonths() * 30;
							Date planRenewalDate = DateService.addDaysToDate(noOfDays);
							user.setPlanRenewalDate(planRenewalDate);
							user.setAmountPaidForPresentPlan(price);
							userService.updateUser(user);
						} else {
							LOGGER.info("Something has gone wrong!");
						}
					}
				}
			}
		}
	}

	public User changePlan(int userId, int planId) {
		User user = userService.getUser(userId);
		Plan plan = planService.getPlan(planId);
		int noOfDays = plan.getDurationInMonths() * 30;
		Date newPlanRenewalDate = DateService.addDaysToDate(noOfDays);
		user.setPlan(plan);
		user.setPlanRenewalDate(newPlanRenewalDate);
		user.setAmountPaidForPresentPlan(plan.getPrice());
		userService.updateUser(user);
		return user;
	}

	public boolean cancelPlan(int userId) {
		User user = null;
		try {
			user = userService.getUser(userId);
			Plan plan = user.getPlan();
			int planId = plan.getId();
			if (isPlan(planId)) {
				user.setPlan(null);
				user.setPlanRenewalDate(null);
				user.setAmountPaidForPresentPlan(0);
				userService.updateUser(user);
				return true;
			} else {
				user.setPlan(null);
				user.setNextSubscriptionPlan(null);
				user.setFreeTrialExpiryDate(null);
				user.setFreeTrialPromoCode(null);
				userService.updateUser(user);
				return true;
			}
		} catch (Exception e) {
			LOGGER.error(e);
			return false;
		}
	}
}
