package com.wavelabs.pricingplansandpromotions.repository;

import org.springframework.data.repository.CrudRepository;

import com.wavelabs.pricingplansandpromotions.model.ZipCode;

public interface ZipCodeRepository extends CrudRepository<ZipCode, Integer> {

}
