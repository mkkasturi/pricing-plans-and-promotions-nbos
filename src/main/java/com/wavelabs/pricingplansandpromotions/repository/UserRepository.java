package com.wavelabs.pricingplansandpromotions.repository;

import org.springframework.data.repository.CrudRepository;

import com.wavelabs.pricingplansandpromotions.model.User;

public interface UserRepository extends CrudRepository<User, Integer> {

	User findByUuidAndTenantId(String uuid, String tenantId);
	
}
